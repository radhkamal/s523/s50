import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard ({courseProp}) {

	// console.log(props);
	// console.log(typeof props);

	console.log(courseProp);
	const { name, description, price, _id} = courseProp;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll () {

		setCount(count + 1);
		console.log('Enrollees' + 1);

		setSeats(seats - 1);
		console.log('Enrollees' + 1);
	}

	useEffect(() => {

		if (seats === 0) {
			alert("No more seats available.");
		}
	}, [seats]);

	return (
		<Row className="mb-3">
			<Col>
				<Card>
					<Card.Title className="p-3">{name}</Card.Title>

				  	<Card.Body>
				    
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>
				    	{description}
				    </Card.Text>

				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>
				    	Php {price}
				    </Card.Text>

				    <Card.Text>
				    	Enrollees: {count}
				    </Card.Text>

				    <Card.Text>
				    	Seats: {seats}
				    </Card.Text>

				    // <Button variant="primary" onClick={enroll}>Enroll</Button>
				    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)

}