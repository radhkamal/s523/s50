import {Fragment} from 'react'; // hindi magkaerror pag multiple components ang irereturn
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';


import Banner from './components/Banner';
import Highlights from './components/Highlights';

import Courses from './pages/Courses';
import CourseView from './pages/CourseView'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import './App.css';


// function App() {
//   return (
//     <Fragment>
//           <AppNavbar />
//         <Container>
//           <Banner />
//           <Highlights />
//         </Container>
//     </Fragment>
//   );
// }


function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  }); 

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect( () => {
      // console.log(user)
      // console.log(localStorage)

      fetch("http://localhost:4000/users/details", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      })
      .then( res => res.json())
      .then( data => {

        if(typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })

        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })

    }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}></Route>
              <Route exact path="/courses" element={<Courses/>}></Route>
              <Route exact path="/courses/:courseId" element={<CourseView/>} />
              <Route exact path="/login" element={<Login/>}></Route>
              <Route exact path="/logout" element={<Logout/>}></Route>
              <Route exact path="/register" element={<Register/>}></Route>
              <Route exact path="*" element={<Error/>}/>
            </Routes>
          </Container>
      </Router>
    </UserProvider>
  )
}

export default App;
