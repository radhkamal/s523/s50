import { useState, useEffect, useContext } from 'react';
import { Navigate, Redirect } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login () {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	function authenticate(e) {

		e.preventDefault();

		/*
		Syntax:
		    fetch('url', {options})
		    .then(res => res.json())
		    .then(data => {})

		*/
		fetch('http://localhost:4000/users/login', {
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email,
		        password: password
		    })

		})
		.then(res => res.json())
		.then(data => {

		    //console.log(data)
		    //console.log(data.access)

		    if(typeof data.access !== "undefined") {

		        localStorage.setItem('token', data.access);
		        retrieveUserDetails(data.access);

		        Swal.fire({
		            title: "Login Successful",
		            icon: "success",
		            text: "Welcome to Zuitt!"
		        })

		    } else {

                     Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your login details and try again!"
                     })   
            }

		})


		localStorage.setItem('email', email);

		setUser({
			email: localStorage.getItem('email')
		});

		setEmail('');
		setPassword('');

		alert('You are now logged in!');
	}

	const retrieveUserDetails = (token) => {

	            fetch('http://localhost:4000/users/details', {
	                headers: {
	                    Authorization: `Bearer ${token}`
	                }
	            })
	            .then(res => res.json())
	            .then( data => {

	                //console.log(data)
	                //console.log(data._id)
	                //console.log(data.isAdmin)


	                setUser({
	                    id: data._id,
	                    isAdmin: data.isAdmin
	                })

	            })
	        }


	useEffect(() => {

		if(email !== '' && password !== '') {
			
			setIsActive(true);

		} else {

			setIsActive(false);

		}
	}, [email, password]);

	return (

		(user.id != null) ? 
			<Navigate to ='/courses' />
		:

		<Form onSubmit = {(e) => authenticate(e)}>

			<h1>Login</h1>

		  <Form.Group className="mb-3" controlId="email">

		    <Form.Label>Email address</Form.Label>

		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email" 
		    	value = {email}
		    	onChange = {(e) => setEmail(e.target.value)}
		    	required />
		    
		    <Form.Text className="text-muted">
		      	We'll never share your email with anyone else.
		    </Form.Text>

		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    
		    <Form.Label>Password</Form.Label>

		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value = {password}
		    	onChange = {(e) => setPassword(e.target.value)}
		    	password />
		  </Form.Group>

		  {
		  	isActive ?
		  	<Button variant="primary" type="submit" id="submitBtn">
		  	 	Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  		Submit
		  	</Button>
		  }

		</Form>

	)
}