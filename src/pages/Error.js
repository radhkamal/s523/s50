import React from 'react'
import Banner from '../components/Banner'

function Error(props) {
	return (
		<Banner isError = {true} />
	);
};

export default Error;
